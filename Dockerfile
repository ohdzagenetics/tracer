# Header 
FROM openjdk:8-jdk

ENV TRACER_VERSION "1.6"

# Update cache and get wget
RUN apt-get update && \
	apt-get install -qqy \
	tar \
	x11-apps

# Get Tracer
ADD http://tree.bio.ed.ac.uk/download.php?id=88&num=3 tracer.tgz

# Unpack Tracer
RUN mkdir /src/ && \
	tar zxvf tracer.tgz -C /src/ --strip-components=1 && \
	chmod +x -R /src/

# Add to system search paths
ENV PATH="/src/bin/:$PATH" \
	LD_LIBRARY_PATH="/src/lib/:$LD_LIBRARY_PATH"

CMD tracer
